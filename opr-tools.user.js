// ==UserScript==
// @name         OPR tools
// @namespace    https://opr.ingress.com/recon
// @version      0.10.020
// @description  Various improvements to the OPR page.
// @author       1110101, tehstone, Hedger, Deep-thot, senfomat, pd1254, pieter.schutz
// @match        https://opr.ingress.com/recon
// @grant        unsafeWindow

// ==/UserScript==

// source https://gitlab.com/1110101/opr-tools
// merge-requests welcome

/*
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

function addGlobalStyle(css) {
	let head, style;
	head = document.getElementsByTagName("head")[0];
	if (!head) { return; }
	style = document.createElement("style");
	style.type = "text/css";
	style.innerHTML = css;
	head.appendChild(style);
}

function init() {
	const w = typeof unsafeWindow == "undefined" ? window : unsafeWindow;
	let tryNumber = 5;
	const initWatcher = setInterval(function () {
	if (tryNumber === 0) {
		clearInterval(initWatcher);
		w.document.getElementById("NewSubmissionController").insertAdjacentHTML("afterBegin", `
<div class='alert alert-danger'><strong><span class='glyphicon glyphicon-remove'></span> OPR tools initialization failed,</strong> check developer console for error details</div>
`);
		return;
	}
	if (w.angular) {
		let err = false;
		try {
			initAngular();
			clearInterval(initWatcher);
		}
		catch (error) {
			err = error;
			console.log(error);
		}
		if (!err) {
			try {
				initScript();
			} catch (error) {
				console.log(error);
			}
		}
	}
	tryNumber--;
}, 500);

	function initAngular() {
		const el = w.document.querySelector("[ng-app='portalApp']");
		w.$app = w.angular.element(el);
		w.$injector = w.$app.injector();
		w.$rootScope = w.$app.scope();

		w.$scope = function (element) {
			return w.angular.element(element).scope();
		};
	}

	function initScript() {
		const descDiv = document.getElementById("descriptionDiv");
        const ansController = w.$scope(descDiv).answerCtrl;
        const subController = w.$scope(descDiv).subCtrl;
		const scope = w.$scope(descDiv);
        const pageData = subController.pageData;
		let watchAdded = false;
        
		// run on init
		modifyPage();

		if (!watchAdded) {
			// re-run on data change
			scope.$watch("subCtrl.pageData", function () {
				modifyPage();
			});
		}

		function modifyPage() {
            if (pageData != null) {
			// adding CSS
			addGlobalStyle(`
body {
font-variant-ligatures: none;
}
.dropdown {
position: relative;
display: inline-block;
}

.dropdown-content {
display: none;
position: absolute;
z-index: 1;
margin: 0;
}
.dropdown-menu li a {
color: #ddd !important;
}
.dropdown:hover .dropdown-content {
display: block;
background-color: #004746 !important;
border: 1px solid #0ff !important;
border-radius: 0px !important;

}
.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
background-color: #008780;
}
.modal-sm {
width: 350px !important;
}

/**
* Tooltip Styles
*/

/* Add this attribute to the element that needs a tooltip */
[data-tooltip] {
position: relative;
z-index: 2;
cursor: pointer;
}

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
visibility: hidden;
-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
opacity: 0;
pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
position: absolute;
top: 150%;
left: 50%;
margin-bottom: 5px;
margin-left: -80px;
padding: 7px;
width: relative;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
background-color: #000;
background-color: hsla(0, 0%, 20%, 0.9);
color: #fff;
content: attr(data-tooltip);
text-align: center;
font-size: 14px;
line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
position: absolute;
top: 132%;
left: relative;
width: 0;
border-bottom: 5px solid #000;
border-bottom: 5px solid hsla(0, 0%, 20%, 0.9);
border-right: 5px solid transparent;
border-left: 5px solid transparent;
content: " ";
font-size: 0;
line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
visibility: visible;
-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
opacity: 1;
}
`);
            
			// adding map buttons
			const mapButtons = [
				"<a class='button btn btn-default' target='_blank' href='https://www.ingress.com/intel?ll=" + pageData.lat + "," + pageData.lng + "&z=17'>Intel</a>",
				"<a class='button btn btn-default' target='_blank' href='https://www.openstreetmap.org/?mlat=" + pageData.lat + "&mlon=" + pageData.lng + "&zoom=16'>OSM</a>",
				"<a class='button btn btn-default' target='_blank' href='https://bing.com/maps/default.aspx?cp=" + pageData.lat + "~" + pageData.lng + "&lvl=16&style=a'>bing</a>"
			];

			// more map buttons in a dropdown menu
			const mapDropdown = [
			];

			descDiv.insertAdjacentHTML("beforeEnd", "<div><div class='btn-group'>" + mapButtons.join("") +
					"<div class='button btn btn-primary dropdown'><span class='caret'></span><ul class='dropdown-content dropdown-menu'>" + mapDropdown.join("") + "</div></div>");


            // moving submit button to right side of classification-div
            const submitDiv = w.document.querySelectorAll("#submitDiv, #submitDiv + .text-center");
            const classificationRow = w.document.querySelector(".classification-row");
            const newSubmitDiv = w.document.createElement("div");
            newSubmitDiv.className = "col-xs-12 col-sm-6";
			submitDiv[0].style.marginTop = 16;
            newSubmitDiv.appendChild(submitDiv[0]);
            newSubmitDiv.appendChild(submitDiv[1]);
            classificationRow.insertAdjacentElement("afterend", newSubmitDiv);

            newSubmitDiv.insertAdjacentHTML("afterbegin", "<div class='center' style='text-align: center' data-tooltip='Use this as a general guide, not perfectly accurate. Refreshing the page will reset only the visible timer.'>Review Time Remaining = <span id='timer'></span></div>");
            w.document.getElementById('timer').innerHTML = 20 + ":" + 00;
            startTimer();

			// adding text buttons
			const textButtons = [
				"<button id='photo' class='button btn btn-default textButton' data-tooltip='indicates a low quality photo'>Photo</button>",
				"<button id='private' class='button btn btn-default textButton' data-tooltip='located on private residential property'>Private</button>",
				"<button id='ems' class='button btn btn-default textButton' data-tooltip='portal candidate interferes with EMS operations'>EMS</button>",
				"<button id='school' class='button btn btn-default textButton' data-tooltip='located on school property'>School</button>",
				"<button id='person' class='button btn btn-default textButton' data-tooltip='photo contains 1 or more people'>Person</button>",
				"<button id='perm' class='button btn btn-default textButton' data-tooltip='seasonal or temporary display or item'>Temporary</button>",
				"<button id='location' class='button btn btn-default textButton' data-tooltip='incorrect portal location'>Location</button>",
				"<button id='clear' class='button btn btn-default textButton' data-tooltip='clears the comment box'>Clear</button>"
			];

            newSubmitDiv.insertAdjacentHTML("beforeEnd", "<div class='center' style='text-align: center'>" + textButtons.join("") + "</div>");

            const textBox = w.document.querySelector("#submitDiv + .text-center > textarea");

            var commentText = "";
            var clickHandle = function(){
            const source = event.target || event.srcElement;
						let text;
						switch (source.id) {
							case "photo":
								text = "low quality photo";
								break;
							case "private":
								text = "private residential property";
								break;
							case "ems":
								text = "portal candidate interferes with Police, Fire, EMT, or other emergency operations";
								break;
							case "school":
								text = "located on primary or secondary school grounds";
								break;
							case "person":
								text = "picture contains one or more people";
								break;
							case "perm":
								text = "portal candidate is seasonal or temporary";
								break;
							case "location":
								text = "Portal candidate's location is not on object or cannot be located";
								break;
							case "clear":
                                commentText = '';
								text = "";
								break;
						}
						if (!commentText) {
                    commentText = text;
                } else {
                    commentText = commentText + ", " + text;
                }
                textBox.innerText = commentText;
            };
        
			const buttons = w.document.getElementsByClassName("textButton");
			for (let b in buttons) {
				if (buttons.hasOwnProperty(b)) {
					buttons[b].addEventListener("click", clickHandle, false);
				}
			}


			// adding percent procressed number
			const stats = w.document.querySelector("#player_stats").children[2];

			const reviewed = parseInt(stats.children[3].children[2].innerText);
			const accepted = parseInt(stats.children[5].children[2].innerText);
			const rejected = parseInt(stats.children[7].children[2].innerText);
            agreed = accepted + rejected;

			let percent = (accepted + rejected) / reviewed;
			percent = Math.round(percent * 1000) / 10;
			tiercounts = [100, 750, 2500, 5000, 10000];
			tiers = ["Bronze", "Silver", "Gold", "Platinum", "Onyx"];
			nexttier = 0;
            completion = 0;
            remaining = 0;
            bestcase = 0;

			for (var i = 0; i < tiercounts.length; i++) {
				if(agreed < tiercounts[i]) {
					nexttier = i;
					break;
				}
			}
            
            if (agreed >= 10000) {
                nexttier = 4;
            }
            
            tier = "";
            tiercount = "";
            mid = "";
            
            if (agreed < 10000) {
            	tier = tiers[nexttier] + " ";
            	tiercount = tiercounts[nexttier];
            	mid = ' / ';
                completion = tiercount /  percent;
                completion = Math.round(completion * 100);
                remaining = completion - reviewed;
                bestcase = tiercount / reviewed;
                bestcase = Math.round(bestcase * 1000) / 10;
            }
            
            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Total number of portal candidates that have been created or rejected based on your analysis.">'+ tier + 'Progress (Agreement %):</span> <span class="gold pull-right">' + agreed + mid + tiercount + ' (' + percent + '%)</span></p>');
	        if (agreed < 10000) {
	            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
	                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Estimated total review count needed for the next badge tier. This is a rough estimate.">Completion Estimate:</span> <span class="gold pull-right">' + completion  + ' (' + remaining + ')' + '</span></p>');
	            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
	                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Required agreement percentage for next badge tier with no more reviews completed.">Best Case:</span> <span class="gold pull-right">' + bestcase + '%</span></p>');
            }

			// portal image zoom button with "=s0"
			w.document.querySelector("#AnswersController .ingress-background").insertAdjacentHTML("beforeBegin",
					"<div style='position:absolute;float:left;'><a class='button btn btn-default' style='display:inline-block;' href='" + subController.pageData.imageUrl + "=s0' target='_blank'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></div>");

			// skip "Your analysis has been recorded." dialog and go directly to next review
			exportFunction(function () {
				window.location.assign("/recon");
			}, ansController, {defineAs: "openSubmissionCompleteModal"});

			// Make photo filmstrip scrollable
			const filmstrip = w.document.getElementById("map-filmstrip");

			function scrollHorizontally(e) {
				e = window.event || e;
				const delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
				filmstrip.scrollLeft -= (delta * 50); // Multiplied by 50
				e.preventDefault();
			}

			filmstrip.addEventListener("DOMMouseScroll", scrollHorizontally, false);
			filmstrip.addEventListener("mousewheel", scrollHorizontally, false);

			subController.map.setZoom(16);

			// Re-enabling scroll zoom
			subController.map.setOptions(cloneInto({scrollwheel: true}, w));

			// HACKY way to move portal rating to the right side
			const scorePanel = w.document.querySelector("div[class~='pull-right']");
			let nodesToMove = Array.from(w.document.querySelector("div[class='btn-group']").parentElement.children);
			nodesToMove = nodesToMove.splice(2, 6);
			nodesToMove.push(w.document.createElement("br"));
			for (let j = nodesToMove.length - 1; j >= 0; --j) {
				scorePanel.insertBefore(nodesToMove[j], scorePanel.firstChild);
			}

			// Bind click-event to Dup-Images-Filmstrip. result: a click to the detail-image the large version is loaded in another tab
            const imgDups = w.document.querySelectorAll("#map-filmstrip > ul > li > img");
            const clickListener = function () {
	            w.open(this.src + "=s0", '_blank');
            };
            for (let imgSep in imgDups) {
                if (imgDups.hasOwnProperty(imgSep)) {
                    imgDups[imgSep].addEventListener("click", function () {
                        const imgDup = w.document.querySelector("#content > img");
                        if (imgDup != null) {
                            imgDup.removeEventListener("click", clickListener);
                            imgDup.addEventListener("click", clickListener);
                            imgDup.setAttribute("style", "cursor: pointer;");
                        }
                    });
                }
            }

			// Automatically open the first listed possible duplicate
			try {
				const e = w.document.querySelector("#map-filmstrip > ul > li:nth-child(1) > img");
                if (e != null) {
                    setTimeout(function () {
                        e.click();
                    }, 500);
                }
			} catch (err) {}

			// expand automatically the "What is it?" filter text box
			try {
				const f = w.document.querySelector("#AnswersController > form > div:nth-child(5) > div > p > span.ingress-mid-blue.text-center");
				setTimeout(function () {
					f.click();
				}, 500);
			} catch (err) {}

            // keyboard navigation
            // keys 1-5 to vote
            // space/enter to confirm dialogs
            // esc or numpad "/" to reset selector
            // Numpad + - to navigate

            let currentSelectable = 0;
            let maxItems = 9;

            function highlight() {
                w.document.querySelectorAll('.btn-group').forEach(exportFunction((element) => { element.style.border = 'none'; }, w));
                if(currentSelectable < maxItems) {
                    w.document.querySelectorAll('.btn-group')[currentSelectable+1].style.border = cloneInto('1px dashed #ebbc4a', w);
                }
            }

            addEventListener('keydown', (event) => {

                /*
				keycodes:

				8: Backspace
				9: TAB
				13: Enter
                16: Shift
				27: Escape
				32: Space
                65: a (open portal image)
                68: d (duplicate)
				107: NUMPAD +
				109: NUMPAD -
				111: NUMPAD /

				49 - 53:  Keys 1-5
				97 - 101: NUMPAD 1-5

				 */

                if(event.keyCode >= 49 && event.keyCode <= 53)
                    numkey = event.keyCode - 48;
                else if(event.keyCode >= 97 && event.keyCode <= 101)
                    numkey = event.keyCode - 96;
                else
                    numkey = null;

                if(w.document.querySelector("input[type=text]:focus") || w.document.querySelector("textarea:focus")) {
                    return;
                }
                
                // open portal image
                else if((event.keyCode === 65) && w.document.querySelector('#AnswersController > form > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a')) {
                    w.document.querySelector('#AnswersController > form > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a').click();
                    event.preventDefault();
                } // "analyze next" button
                else if((event.keyCode === 13 ||event.keyCode === 32) && w.document.querySelector('a.button[href="/recon"]')) {
                    w.document.location.href='/recon';
                    event.preventDefault();
                    
                } // submit low quality rating
                else if((event.keyCode === 13 ||event.keyCode === 32) && w.document.querySelector('[ng-click="answerCtrl2.confirmLowQuality()"]')) {
                    w.document.querySelector('[ng-click="answerCtrl2.confirmLowQuality()"]').click();
                    currentSelectable = 0;
                    event.preventDefault();

                } // click duplicate
                else if((event.keyCode === 68) && w.document.querySelector('#content > button')) {
                    w.document.querySelector('#content > button').click();
                    currentSelectable = 0;
                    event.preventDefault();
                    
                } // submit duplicate
                else if((event.keyCode === 13 ||event.keyCode === 32) && w.document.querySelector('[ng-click="answerCtrl2.confirmDuplicate()"]')) {
                    w.document.querySelector('[ng-click="answerCtrl2.confirmDuplicate()"]').click();
                    currentSelectable = 0;
                    event.preventDefault();

                } // submit normal rating
                else if((event.keyCode === 13 ||event.keyCode === 32) && currentSelectable === maxItems) {
                    w.document.querySelector('[ng-click="answerCtrl.submitForm()"]').click();
                    event.preventDefault();

                } // close duplicate dialog
                else if((event.keyCode === 27 || event.keyCode === 111) && w.document.querySelector('[ng-click="answerCtrl2.resetDuplicate()"]')) {
                    w.document.querySelector('[ng-click="answerCtrl2.resetDuplicate()"]').click();
                    currentSelectable = 0;
                    event.preventDefault();

                } // close low quality ration dialog
                else if((event.keyCode === 27 || event.keyCode === 111) && w.document.querySelector('[ng-click="answerCtrl2.resetLowQuality()"]')) {
                    w.document.querySelector('[ng-click="answerCtrl2.resetLowQuality()"]').click();
                    currentSelectable = 0;
                    event.preventDefault();
                }
                // return to first selection (should this be a portal)
                else if(event.keyCode === 27 || event.keyCode === 111) {
                    currentSelectable = 0;
                }
                // select next rating
                else if((event.keyCode === 107 || event.keyCode === 9) && currentSelectable < maxItems) {
                    currentSelectable++;
                   
                    event.preventDefault();
                }
                // select previous rating
                else if((event.keyCode === 109 || event.keyCode === 16 || event.keyCode === 8) && currentSelectable > 0) {
                    currentSelectable--;
                    event.preventDefault();

                }
                else if(numkey === null || currentSelectable >= maxItems) {
                    return;
                }
                // rating 1-5
                else {
                    w.document.querySelectorAll('.btn-group')[currentSelectable+1].querySelectorAll('button.button-star')[numkey-1].click();
                    currentSelectable++;
                }
                if (currentSelectable === 4) {
                    	currentSelectable = 7;
                }
                if (currentSelectable === 6) {
                    	currentSelectable = 3;
                }
                highlight();
            });

            highlight();
			watchAdded = true;
		}

	}
    }
    function startTimer() {
  var presentTime = document.getElementById('timer').innerHTML;
  var timeArray = presentTime.split(/[:]+/);
  var m = timeArray[0];
  var s = checkSecond((timeArray[1] - 1));
  if(s==59){m=m-1;}
  
  document.getElementById('timer').innerHTML =
    m + ":" + s;
  setTimeout(startTimer, 1000);
}

function checkSecond(sec) {
  if (sec < 10 && sec >= 0) {sec = "0" + sec;} // add zero in front of numbers < 10
  if (sec < 0) {sec = "59";}
  return sec;
}
}

setTimeout(function () {
	if (document.querySelector("[src*='all-min']")) {
		init();
	}
}, 500);
