# OPR Tools
Userscript for Ingress Operation Portal Recon - https://opr.ingress.com/recon

![](./image/opr-tools.png)
![](./image/opr-tools-2.png)

Features:
- Additional links to map services like Intel, OpenStreetMap, bing and some international ones
- Disabled annoying automatic page scrolling
- Removed "Your analysis has been recorded." dialog
- Automatically opens the first listed possible duplicate
- Automatically opens the "What is it?" filter text box
- Buttons below the comments box to auto-type common 1-star rejection reasons
- Total agreement count, agreement percentage, and completion estimate added to stats dropdown
- Option to open portal image in new tab for current candidate and all possible duplicates
- Moved overall portal rating to same group as other ratings
- Made "Nearby portals" list and map scrollable with mouse wheel
- Keyboard navigation:
    * 1-5 will select star rating and automatically move to the next rating category
    * Shift and Tab to navigate between rating categories
    * Enter to submit review or accept dialog options
    * Escape to cancel dialog options
    * 'd' to choose currently selected duplicate option
    * Ctrl to open portal image in new tab

Download: https://gitlab.com/tehstone/opr-tools/raw/master/opr-tools.user.js