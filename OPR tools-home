// ==UserScript==
// @name         OPR tools-home
// @namespace    https://opr.ingress.com
// @version      0.2.5
// @description  Added OPR stats to homepage
// @author       tehstone
// @match        https://opr.ingress.com
// @grant        unsafeWindow

// ==/UserScript==

// merge-requests welcome

/*
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

function addGlobalStyle(css) {
	let head, style;
	head = document.getElementsByTagName("head")[0];
	if (!head) { return; }
	style = document.createElement("style");
	style.type = "text/css";
	style.innerHTML = css;
	head.appendChild(style);
}

function init() {
	const w = typeof unsafeWindow == "undefined" ? window : unsafeWindow;
	let tryNumber = 5;
	const initWatcher = setInterval(function () {
	if (tryNumber === 0) {
		clearInterval(initWatcher);
		w.document.getElementById("NewSubmissionController").insertAdjacentHTML("afterBegin", `
<div class='alert alert-danger'><strong><span class='glyphicon glyphicon-remove'></span> OPR tools initialization failed,</strong> check developer console for error details</div>
`);
		return;
	}
	if (w.angular) {
		let err = false;
		try {
			initAngular();
			clearInterval(initWatcher);
		}
		catch (error) {
			err = error;
			console.log(error);
		}
		if (!err) {
			try {
				initScript();
			} catch (error) {
				console.log(error);
			}
		}
	}
	tryNumber--;
}, 500);

	function initAngular() {
		const el = w.document.querySelector("[ng-app='portalApp']");
		w.$app = w.angular.element(el);
		w.$injector = w.$app.injector();
		w.$rootScope = w.$app.scope();

		w.$scope = function (element) {
			return w.angular.element(element).scope();
		};
	}

	function initScript() {
		const descDiv = document.getElementById("descriptionDiv");
		const scope = w.$scope(descDiv);
		let watchAdded = false;

		// run on init
		modifyPage();

		//if (!watchAdded) {
			// re-run on data change
		//	scope.$watch("subCtrl.pageData", function () {
		//		modifyPage();
		//	});
		//}

		function modifyPage() {

			// adding CSS
			addGlobalStyle(`
.dropdown {
position: relative;
display: inline-block;
}

.dropdown-content {
display: none;
position: absolute;
z-index: 1;
margin: 0;
}
.dropdown-menu li a {
color: #ddd !important;
}
.dropdown:hover .dropdown-content {
display: block;
background-color: #004746 !important;
border: 1px solid #0ff !important;
border-radius: 0px !important;

}
.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
background-color: #008780;
}
.modal-sm {
width: 350px !important;
}

/**
* Tooltip Styles
*/

/* Add this attribute to the element that needs a tooltip */
[data-tooltip] {
position: relative;
z-index: 2;
cursor: pointer;
}

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
visibility: hidden;
-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
opacity: 0;
pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
position: absolute;
top: 150%;
left: 50%;
margin-bottom: 5px;
margin-left: -80px;
padding: 7px;
width: relative;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
background-color: #000;
background-color: hsla(0, 0%, 20%, 0.9);
color: #fff;
content: attr(data-tooltip);
text-align: center;
font-size: 14px;
line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
position: absolute;
top: 132%;
left: relative;
width: 0;
border-bottom: 5px solid #000;
border-bottom: 5px solid hsla(0, 0%, 20%, 0.9);
border-right: 5px solid transparent;
border-left: 5px solid transparent;
content: " ";
font-size: 0;
line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
visibility: visible;
-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
opacity: 1;
}
`);
			// adding percent procressed number
			const stats = w.document.querySelector("#player_stats").children[2];

			const reviewed = parseInt(stats.children[3].children[2].innerText);
			const accepted = parseInt(stats.children[5].children[2].innerText);
			const rejected = parseInt(stats.children[7].children[2].innerText);
            agreed = accepted + rejected;

			let percent = (accepted + rejected) / reviewed;
			percent = Math.round(percent * 1000) / 10;
			tiercounts = [100, 750, 2500, 5000, 10000];
			tiers = ["Bronze", "Silver", "Gold", "Platinum", "Onyx"];
			nexttier = 0;
            completion = 0;
            bestcase = 0;
            remaining = 0;
            
			for (var i = 0; i < tiercounts.length; i++) {
				if(agreed < tiercounts[i]) {
					nexttier = i;
					break;
				}
			}
            
            if (agreed >= 10000) {
                nexttier = 4;
            }
            
            tier = "";
            tiercount = "";
            mid = "";
            
            if (agreed < 10000) {
            	tier = tiers[nexttier] + " ";
            	tiercount = tiercounts[nexttier];
            	mid = ' / ';
                completion = tiercount /  percent;
                completion = Math.round(completion * 100);
                remaining = completion - reviewed;
                bestcase = tiercount / reviewed;
                bestcase = Math.round(bestcase * 1000) / 10;
            }
            
            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Total number of portal candidates that have been created or rejected based on your analysis.">'+ tier + 'Progress (Agreement %):</span> <span class="gold pull-right">' + agreed + mid + tiercount + ' (' + percent + '%)</span></p>');
	        if (agreed < 10000) {
	            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
	                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Estimated total review count needed for the next badge tier. This is a rough estimate.">Completion Estimate:</span> <span class="gold pull-right">' + completion  + ' (' + remaining + ')' + '</span></p>');
	            w.document.querySelector("#player_stats:not(.visible-xs) div p:last-child")
	                .insertAdjacentHTML("afterEnd", '<br><p><span class="glyphicon glyphicon-info-sign ingress-gray pull-left"></span><span style="margin-left: 5px" class="ingress-mid-blue pull-left" data-tooltip="Required agreement percentage for next badge tier with no more reviews completed.">Best Case:</span> <span class="gold pull-right">' + bestcase + '%</span></p>');
            }
            
			watchAdded = true;
		}

	}

}

setTimeout(function () {
	if (document.querySelector("[src*='all-min']")) {
		init();
	}
}, 500);
